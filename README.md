# OrangeFox device beryllium
For building TWRP for Xiaomi Poco F1 ONLY

To compile please see OrangeFox manifest

lunch omni_beryllium-eng

The Redmi Note 5 Pro (codenamed _"beryllium"_) are high-end smartphones from Xiaomi.

## Device specifications

| Device       | Xiaomi Redmi Note 5 Pro                                             |
| -----------: | :-------------------------------------------------------------------|
| SoC          | Qualcomm Snapdragon 845                                             |
| CPU          | Octa-core, 4x2.8 GHz Kryo 385 Gold & 4x1.7 GHz Kryo 385             |
| GPU          | Adreno 630                                                          |
| Memory       | 6GB / 8GM RAM (LPDDR4X)                                             |
| Shipped Android version | 7.1.1                                                    |
| Storage      | 64/128/256GB UFS 2.1                                                |
| Battery      | Non-removable Li-Po 4000 mAh                                        |
| Dimensions   | 158.6 x 75.4 x 8.05 mm                                              |
| Display      | 2160 x 1080 (18:9), 5.99 inch                                       |
| Rear camera 1 | 12MP, 1.9-micron pixels, f/1.9 Dual LED flash                     |
| Rear camera 2 | 5MP, 1.12-micron pixels, f/2.0                                     |
| Front camera | 20MP, 1.8-micron pixels, f/2.8 1080p 30 fps video       |
